from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from . import models


def index(request):
    ctx = {
        'all_merchants': models.Merchant.objects.all(),
    }
    return render(request, 'shopping_cart_app/index.html', ctx)

def merchant_index(request, merchant_name):
    try:
        merchant = models.Merchant.objects.get(name=merchant_name)
        ctx =  {
            'all_products': merchant.product_set.all(),
            'merchant': merchant,
        }
        return render(request, 'shopping_cart_app/merchant_index.html', ctx)
    except:
        return HttpResponse('{} does not exist'.format(merchant_name))

def merchant_product(request, merchant_name, product_id):
    merchant = None
    product = None
    try:
        merchant = models.Merchant.objects.get(name=merchant_name)
    except:
        return HttpResponse('{} does not exist'.format(merchant_name))
    try:
        product = merchant.product_set.get(id=product_id)
    except:
        return HttpResponse(
            'No product with id={} for {}'.format(product_id, merchant_name)
        )
    ctx = {
        'product': product,
        'merchant': merchant,
        'product_id': product_id,
    }
    return render(request, 'shopping_cart_app/merchant_product.html', ctx)

@login_required
def add_item_to_cart(request, merchant_name, product_id):
    if request.method == 'POST':
        quantity = None
        try:
            quantity = int(request.POST['quantity'])
        except:
            pass
        if quantity is None:
            return HttpResponse('invalid quantity')
        customer = models.Customer.objects.get(user=request.user)
        e = models.ShoppingCartEntry(
            customer=customer,
            product=models.Product.objects.get(id=product_id),
            quantity=quantity
        )
        e.save()
        return redirect(merchant_index, merchant_name=merchant_name)

@login_required
def checkout(request, merchant_name):
    customer = models.Customer.objects.get(user=request.user)
    entries = customer.shoppingcartentry_set.all()
    ctx = {
        'entries': entries,
        'merchant': customer.merchant,
        'balance': customer.get_shopping_cart_balance(),
    }
    return render(request, 'shopping_cart_app/checkout_page.html', ctx)

@login_required
def buy(request, merchant_name):
    if request.method == 'POST':
        customer = models.Customer.objects.get(user=request.user)
        customer.checkout_shopping_cart()
        return redirect(merchant_index, merchant_name=merchant_name)

@login_required
def show_past_orders(request, merchant_name):
    customer = models.Customer.objects.get(user=request.user)
    ctx = {
        'all_orders': customer.order_set.all(),
        'merchant': customer.merchant,
    }
    return render(request, 'shopping_cart_app/order_history_list.html', ctx)

@login_required
def order_details(request, merchant_name, order_id):
    order = models.Order.objects.get(id=order_id)
    ctx = {
        'merchant': order.customer.merchant,
        'entries': order.entries.all(),
        'balance': order.get_balance(),
    }
    return render(request, 'shopping_cart_app/order_details.html', ctx)

@csrf_exempt # massive security flaw, but whatever
def register(request):
    if request.method == 'POST':
        username = request.POST.get('usn', '')
        password = request.POST.get('pwd', '')
        merchant_name = request.POST.get('merch', '')
        try:
            user = User.objects.create_user(username, None, password)
            customer = models.Customer()
            customer.merchant = models.Merchant.objects.get(name=merchant_name)
            customer.user = user
            customer.save()
            login(request, user)
            return redirect(merchant_index, merchant_name=merchant_name)
        except:
            return HttpResponse('username taken')

@csrf_exempt
def do_login(request):
    if request.method == 'POST':
        username = request.POST.get('usn', '')
        password = request.POST.get('pwd', '')
        merchant_name = request.POST.get('merch', '')
        user = authenticate(username=username, password=password)
        if not user:
            return HttpResponse('bad login credentials')
        else:
            login(request, user)
            return redirect(merchant_index, merchant_name=merchant_name)

@login_required
def do_logout(request):
    if request.method == 'POST':
        logout(request)
        return redirect(index)
