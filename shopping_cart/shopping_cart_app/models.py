from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


class Merchant(models.Model):
    name = models.CharField(max_length=256)

    def __str__(self):
        return 'name={} id={}'.format(self.name, self.id)

class Product(models.Model):
    name = models.CharField(max_length=256)
    merchant = models.ForeignKey(Merchant, on_delete=models.CASCADE)
    price = models.FloatField()

    def __str__(self):
        return 'name={} id={}'.format(self.name, self.id)

class Customer(models.Model):
    merchant = models.ForeignKey(Merchant, on_delete=models.CASCADE)
    user = models.OneToOneField(User)

    def __str__(self):
        return '{} {}'.format(self.user.username)

    def get_shopping_cart_balance(self):
        entries = self.shoppingcartentry_set.all()
        return sum(e.product.price*e.quantity for e in entries)

    def checkout_shopping_cart(self):
        order = Order(customer=self, timestamp=timezone.now())
        order.save()
        for e in self.shoppingcartentry_set.all():
            e.customer = None
            order.entries.add(e)
            e.save()
        return order

class ShoppingCartEntry(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, null=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField()


class Order(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    timestamp = models.DateTimeField()
    entries = models.ManyToManyField(ShoppingCartEntry)

    def get_balance(self):
        return sum(e.product.price*e.quantity for e in self.entries.all())
