from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(
        r'^merchant/(?P<merchant_name>\w+)$',
        views.merchant_index,
        name='merchant_index'
    ),
    url(
        r'^merchant/(?P<merchant_name>\w+)/products/(?P<product_id>\d+)$',
        views.merchant_product,
        name='merchant_product'
    ),
    url(
        r'^merchant/(?P<merchant_name>\w+)/products/(?P<product_id>\d+)/a$',
        views.add_item_to_cart,
        name='add_item_to_cart'
    ),
    url(
        r'^merchant/(?P<merchant_name>\w+)/history$',
        views.show_past_orders,
        name='show_past_orders'
    ),
    url(
        r'^merchant/(?P<merchant_name>\w+)/history/(?P<order_id>\d+)$',
        views.order_details,
        name='order_details'
    ),
    url(
        r'^merchant/(?P<merchant_name>\w+)/checkout$',
        views.checkout,
        name='checkout'
    ),
    url(
        r'^merchant/(?P<merchant_name>\w+)/buy$',
        views.buy,
        name='buy'
    ),
    url(r'^register$', views.register, name='register'),
    url(r'^login$', views.do_login, name='login'),
    url(r'^logout$', views.do_logout, name='logout'),
]
