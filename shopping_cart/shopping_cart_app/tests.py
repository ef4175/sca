from django.test import TestCase
from .models import *
from django.contrib.auth.models import User
from django.utils import timezone


class MerchantTests(TestCase):
    pass

class ProductTests(TestCase):
    pass

class CustomerTests(TestCase):
    def test_get_balance(self):
        m = Merchant(name='Walgreens')
        m.save()

        p1 = Product(name='Gillette Razors', merchant=m, price=10.5)
        p2 = Product(name='Tropicana Juice', merchant=m, price=4)
        p1.save()
        p2.save()

        c = Customer(
            merchant=m,
            user=User.objects.create_user(username='bill_smith')
        )
        c.save()

        # empty shopping cart
        self.assertEqual(c.get_shopping_cart_balance(), 0)

        e1 = ShoppingCartEntry(customer=c, product=p1, quantity=2)
        e2 = ShoppingCartEntry(customer=c, product=p2, quantity=10)
        e1.save()
        e2.save()

        self.assertEqual(c.get_shopping_cart_balance(), 61)

    def test_checkout_shopping_cart(self):
        m = Merchant(name='Amazon')
        m.save()

        p1 = Product(name='Monster HDMI Cable', merchant=m, price=1000)
        p2 = Product(name='iPhone 9001', merchant=m, price=9000.1)
        p1.save()
        p2.save()

        c = Customer(
            merchant=m,
            user=User.objects.create_user(username='barren_wuffett')
        )
        c.save()

        e1 = ShoppingCartEntry(customer=c, product=p1, quantity=1)
        e2 = ShoppingCartEntry(customer=c, product=p2, quantity=1)
        e1.save()
        e2.save()

        # should have 2 items in shopping cart
        self.assertEqual(c.shoppingcartentry_set.all().count(), 2)

        order = c.checkout_shopping_cart()

        # should have 0 items in shopping cart after checkout
        self.assertEqual(c.shoppingcartentry_set.all().count(), 0)

        # bought cable and phone => balance=10000.1
        self.assertEqual(order.get_balance(), 10000.1)

class ShoppingCartEntryTests(TestCase):
    pass

class OrderTests(TestCase):
    def test_get_balance(self):
        m = Merchant(name='Goldman_Sachs')
        m.save()

        p1 = Product(name='Credit Default Swap', merchant=m, price=1000000)
        p2 = Product(name='AA Corporate Bond', merchant=m, price=100)
        p1.save()
        p2.save()

        c = Customer(
            merchant=m,
            user=User.objects.create_user(username='michael_burry')
        )
        c.save()

        e1 = ShoppingCartEntry(customer=c, product=p1, quantity=10)
        e2 = ShoppingCartEntry(customer=c, product=p2, quantity=5)
        e1.save()
        e2.save()

        o = Order(customer=c, timestamp=timezone.now())
        o.save()
        o.entries.add(e1)
        o.entries.add(e2)

        self.assertEqual(o.get_balance(), 10000500)
